import sql_template as tsql

from   numpy  import datetime64
from   pandas import to_datetime, DataFrame
from   yaml   import load, SafeLoader


def load_project_config() -> dict:
    """ Read the config.yml file into config dict """
    with open("config.yml", "r") as file:
        config = load(file, Loader=SafeLoader)
    return config


def create_connstrings(config: dict) -> dict:
    connstr_dict = dict()
    for key in config['SqlConfig']['Server'].keys():
        server   = config['SqlConfig']['Server'][key]
        database = config['SqlConfig']['Database'][key]
        connstr_dict[key] = tsql.create_cnxnstr(server, database)
    return connstr_dict


def scheduled_transfers(start: datetime64,
                        end: datetime64,
                        connstrs: dict, 
                        sql_only: bool = False) -> DataFrame:
    """
    Scheduled Transfers By Location
    Source: CarvanaDWHS.funnelEvents.tblScheduledTransfers
    """
    
    query_name = 'scheduled_transfers_agg.sql'
    
    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def completed_activities(start: datetime64,
                         end: datetime64,
                         connstrs: dict, 
                         sql_only: bool = False) -> DataFrame:
    """
    Completed Activities By Location
    Source: OpsGroup.marketops.budgeted_hours_by_dept_snapshot
    """
    
    query_name = 'completed_activity_agg.sql'
    
    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def budgeted_hours(start: datetime64,
                   end: datetime64,
                   connstrs: dict, 
                   sql_only: bool = False) -> DataFrame:
    """
    Staff Hour Budgets By Location
    Source: OpsGroup.marketops.budgeted_hours_by_dept_snapshot
    """
    
    query_name = 'budgeted_hours_by_dept_snapshot.sql'
    
    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def bfp_forecast(start: datetime64,
                 end: datetime64,
                 connstrs: dict,
                 sql_only: bool = False) -> DataFrame:
    """
    BFPs Forecasts By Location
    Source: Analytics.marketing.STCForecast
    """
    query_name = 'bfp_forecast.sql'

    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def hauler_forecast(start: datetime64,
                    end:   datetime64,
                    connstrs: dict,
                    sql_only: bool = False) -> DataFrame:
    """
    Hauler Forecasts By Location
    Source: OpsGroup.marketops.ProjectedHaulers
    """
    query_name = 'hauler_forecast.sql'

    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def staffing_new(start: datetime64,
                 end: datetime64,
                 connstrs: dict,
                 sql_only: bool = False) -> DataFrame:
    """
    Employee Forecasts By Location
    Source: OpsGroup.marketops.Staffing_Newv2
    """
    query_name = 'staffing_mktops.sql'

    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def four_day_availability(start: datetime64,
                          end: datetime64,
                          connstrs: dict,
                          sql_only: bool = False) -> DataFrame:
    """
    Snapshotted market ops resources & availability
    Source: Analytics.marketing.MktOpsN4DAvailability
    """
    query_name = 'mktopsn4davailability.sql'

    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def qm_allmarketsV2(connstrs: dict,
                    sql_only: bool = False) -> DataFrame:
    """
    Full AllMarketsV2 table
    Source: Analytics.marketing.AllMarketsV2
    """
    query_name = 'AllMarketsV2.sql'

    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


def stc_split_locations(connstrs: dict,
                        sql_only: bool = False) -> DataFrame:
    """
    Full STC Split Locations table
    Source: OpsGroup.marketops.STC_Split_Locations_Historical
    """
    query_name = 'stc_split_locations.sql'

    return tsql.return_data_or_sql(sql_only,
                                   connstrs['DWHC-RO'],
                                   query_name,
                                   locals())


if __name__=='__main__':
    start = to_datetime("2021/10/01")
    end   = to_datetime("2021/10/05")

    config = load_project_config()
    connstrs = create_connstrings(config)
    print(scheduled_transfers(start, end, connstrs=connstrs, sql_only=False))
    print("done")
    
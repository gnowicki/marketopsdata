# MarketOps Constraints

**Goal:** Pull active MarketOps data, clean it, and load to an Analytics table for Reporting.

## Data

- Resource
  - Staff
    - Actual, Forecasted
  - Haulers
    - Forecasted
  - Loaders (Need Source)
- Performance
  - BFPs Completed/Scheduled
  - Tasks Completed/Scheduled
- Availability
  - Booked, Bookable, Overbooked

## Output Tables

- Strictly Time-Series Records
  - Date, Location, Resource, Metric, Forecast, etc.

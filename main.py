from numpy import datetime64, arange
from pandas.core.frame import DataFrame
from typing import List

import logging as log
from pandas.core.indexes.datetimes import date_range

from pandas.core.tools.datetimes import to_datetime
import MktOpsData as mktops
import pandas as pd


START = pd.to_datetime("2021/01/01")
END   = pd.to_datetime("2021/11/03")


config = mktops.load_project_config()
cnstrs = mktops.create_connstrings(config)
log.basicConfig(filename='main.log', level=log.INFO)
log.info(f"[New Run]\t{START}\t{END}")

def stc_split_data() -> pd.DataFrame:
    stc_split = mktops.stc_split_locations(connstrs=cnstrs)
    stc_split['TurnedOn']  = stc_split.TurnedOn.apply(pd.to_datetime)
    stc_split['TurnedOff'] = stc_split.TurnedOff.apply(pd.to_datetime)
    return stc_split


def all_markets_data() -> pd.DataFrame:
    all_mkts = mktops.qm_allmarketsV2(connstrs=cnstrs)
    all_mkts = all_mkts.loc[all_mkts.LocationID>0]
    all_mkts.loc['HubLaunchDate'] = all_mkts.HubLaunchDate.apply(pd.to_datetime)
    return all_mkts


def bfp_forecast_data(start: datetime64, end: datetime64, cnstrs: dict) -> List[DataFrame]:
    bfp_forecast = mktops.bfp_forecast(start, end, connstrs=cnstrs)
    bfp_forecast['Version'] = bfp_forecast.Version.apply(pd.to_datetime)
    bfp_forecast = bfp_forecast.loc[bfp_forecast.IsCurrent==1, :]
    
    forecast_gb_cols     = ['City','LocationName','STCType']
    bfp_forecast_current = bfp_forecast.groupby(forecast_gb_cols)['Version'].max()
    bfp_forecast_current = bfp_forecast.merge(bfp_forecast_current, on=list(bfp_forecast_current.reset_index().columns))
    bfp_pivoted          = bfp_forecast_current.pivot(columns='STCType',values='BFPForecast',index=['LocationName','Date'])
    bfp_pivoted.rename(columns={'BFP':'ForecastedBFPs',
                                'Trade':'ForecastedTrades'},
                       inplace=True)

    return [bfp_forecast, bfp_forecast_current, bfp_pivoted]


if __name__=='__main__':
    PULL_DATA = False
    """ Historical Location Splits """
    if PULL_DATA:
        stc_split = stc_split_data()
        stc_split.to_csv("Data/STC_Split.csv", index=False)
    else:
        stc_split = pd.read_csv("Data/STC_Split.csv")

    stc_split["TurnedOn"]  = stc_split.TurnedOn.apply(to_datetime)
    stc_split["TurnedOff"] = stc_split.TurnedOff.apply(to_datetime)

    stc_split_loc = pd.DataFrame()
    for row in stc_split[["LocationID", "TurnedOn", "TurnedOff"]].values:
        loc_id, _start, _end = row[0], row[1], row[2]

        if pd.isnull(_start): _start = START
        if pd.isnull(_end):   _end   = END
        
        stc_dates = date_range(_start, _end)
        _stc_loc = pd.DataFrame()
        _stc_loc["DataDate"]   = stc_dates
        _stc_loc["LocationID"] = loc_id
        _stc_loc["StcSplit"]   = 1

        if stc_split_loc.empty:
            stc_split_loc = _stc_loc
        else:
            stc_split_loc = stc_split_loc.append(_stc_loc)

    """ All Markets Data """
    if PULL_DATA:
        all_mkts = all_markets_data()
        all_mkts.to_csv("Data/All_Mkts.csv", index=False)
    else:
        all_mkts = pd.read_csv("Data/All_Mkts.csv")

    all_mkts = all_mkts.loc[~all_mkts.LocationID.isnull()]

    """ BFP Forecast """
    if PULL_DATA:
        bfp_forecast, bfp_current, bfp_pivoted = bfp_forecast_data(start=START,
                                                                   end=END,
                                                                   cnstrs=cnstrs)
        bfp_forecast.to_csv("Data/BFP_Forecast.csv", index=False)
        bfp_forecast.to_csv("Data/BFP_Current.csv", index=False)
        bfp_forecast.to_csv("Data/BFP_Pivoted.csv")    
    else:
        bfp_forecast = pd.read_csv("Data/BFP_Forecast.csv")
        bfp_current  = pd.read_csv("Data/BFP_Current.csv")
        bfp_pivoted  = pd.read_csv("Data/BFP_Pivoted.csv").set_index(['LocationName', 'Date'])
    
    """ Transfers """
    if PULL_DATA:
        transfers = mktops.scheduled_transfers(START, END, connstrs=cnstrs)
        transfers.to_csv("Data/Transfers.csv", index=False)   
    else:
        transfers = pd.read_csv("Data/Transfers.csv")

    """ Availability """
    if PULL_DATA:
        availability = mktops.four_day_availability(START, END, connstrs=cnstrs)
        availability.to_csv("Data/N4DAvailability.csv")
    else:
        availability = pd.read_csv("Data/N4DAvailability.csv")

    """ Budgeted Hours """
    if PULL_DATA:
        budget_hours = mktops.budgeted_hours(START, END, connstrs=cnstrs)
        budget_hours.to_csv("Data/Budget_Hours.csv", index=False)
    else:
        budget_hours = pd.read_csv("Data/Budget_Hours.csv")

    # Quick location overwrite for match to AllMarketsV2
    budget_hours.loc[budget_hours.Market=='El Paso TX', 'Market'] = 'El Paso'
    
    hours = all_mkts.merge(budget_hours,
                           left_on='LocationDescription',
                           right_on='Market')
    assert len(hours) == len(budget_hours)

    """ Completed Activity """
    if PULL_DATA:
        activity = mktops.completed_activities(START, END, connstrs=cnstrs)
        activity.to_csv("Data/Activity.csv", index=False)
    else:
        activity = pd.read_csv("Data/Activity.csv")
    
    """ Staffing Forecast """
    if PULL_DATA:
        staffing = mktops.staffing_new(START, END, connstrs=cnstrs)
        staffing.to_csv("Data/Staffing.csv", index=False)
    else:
        staffing = pd.read_csv("Data/Staffing.csv")

    # Same location overwrite for match to AllMarketsV2
    staffing["ForecastDate"] = staffing.ForecastDate.apply(to_datetime)
    staffing.loc[staffing.LocationName=='El Paso TX', 'LocationName'] = 'El Paso'
    
    forecast_gb_cols = ['LocationName','StaffingMarket','ForecastDate']
    # By current, the most recent forecast for each date
    staffing_current = staffing.groupby(forecast_gb_cols)['SnapshotDate'].max()
    staffing_current = staffing.merge(staffing_current, on=list(staffing_current.reset_index().columns))
    staffing_current = staffing_current.merge(all_mkts[['LocationDescription', 'LocationID']], left_on='LocationName', right_on='LocationDescription', how='left')
    
    staffing_cols    = ['LocationName', 'StaffingMarket', 'ForecastDate', 'SnapshotDate', 'ForecastedEmployees', 'NumCurrentEmployees', 'NumProductiveEmployees', 'NumInTrainingEmployees','LocationID']
    stc_staff        = staffing_current.loc[staffing_current.JobProfile=='STC Advocate', staffing_cols]
    retail_staff     = staffing_current.loc[staffing_current.JobProfile=='Customer Advocate', staffing_cols]
    

    """ Hauler Forecast """
    if PULL_DATA:
        hauler_forecast = mktops.hauler_forecast(START, END, connstrs=cnstrs)
        hauler_forecast.to_csv("Data/Hauler.csv", index=False)
    else:
        hauler_forecast = pd.read_csv("Data/Hauler.csv")
    
    # Location Overwrites - Not all locations that needed overwrites.  Just the ones that had non-zero values
    hauler_forecast.loc[hauler_forecast.LocationName=='El Paso TX', 'LocationName']              = 'El Paso'
    hauler_forecast.loc[hauler_forecast.LocationName=='Detroit MI - Wyoming', 'LocationName']    = 'Detroit MI - Wyoming '
    hauler_forecast.loc[hauler_forecast.LocationName=='Birmingham - Bessemer', 'LocationName']   = 'Birmingham'
    hauler_forecast.loc[hauler_forecast.LocationName=='Greensboro NC - Auction', 'LocationName'] = 'Greensboro'
    hauler_forecast.loc[hauler_forecast.LocationName=='Colorado Springs CO', 'LocationName']     = 'Colorado Springs'
    hauler_forecast = hauler_forecast.merge(all_mkts[['LocationDescription', 'LocationID']],
                                            left_on='LocationName',
                                            right_on='LocationDescription',
                                            how='left')
    hauler_forecast.rename(columns={'HaulersNeeded':'ForecastedHaulers'}, inplace=True)

    haulers_missed = hauler_forecast.loc[hauler_forecast.LocationID.isnull(),'ForecastedHaulers'].sum()
    if haulers_missed > 0:
        print(f"[WARNING] {haulers_missed} haulers missed due to missing LocationID match in AllMarketsV2")

    dates = pd.DataFrame()
    dates["DataDate"] = pd.date_range(START, END)
    dates["DateId"] = arange(len(dates.DataDate))

    loc_data = all_mkts[["LocationID","LocationDescription","LocationName"]]
    df_idx = pd.DataFrame()
    for idx in range(len(dates.DataDate)):
        _loc_data = loc_data.copy()
        _loc_data["DateId"] = idx
        if df_idx.empty:
            df_idx = _loc_data
        else:
            df_idx = df_idx.append(_loc_data)
    
    table_idx = df_idx.merge(dates)
    
    main = table_idx.merge(staffing,
                           left_on=["DataDate", "LocationName"],
                           right_on=["ForecastDate", "LocationName"],
                           how='left')


from   datetime  import datetime
from   functools import wraps
from   numpy     import datetime64
from   pandas    import Timestamp, DataFrame
from   pandas.io.sql import read_sql_query
from   platform  import system
from   pyodbc    import connect
from   six       import string_types

import logging as log
import os
import time

SQL_DIR  = 'sql_queries'

date_types = (datetime64, Timestamp, datetime)

""" Timing """
def query_timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()

        q_name       = args[1]
        exe_time     = round(te-ts,3)
        rows         = len(result)
        rows_str     = rows_to_str(rows)
        rows_per_str = rows_to_str(rows/exe_time)
        log.info(f'\t[SQL Query]\t{q_name}')
        log.info(f'\t\t\t> {exe_time} s; \t{rows_str} rows; \t{rows_per_str} rows/sec')
        return result
    return wrap


def rows_to_str(num: float) -> str:
    if num >= 1e9:
        return f'{round(num/1e9,1)}B'
    elif num >= 1e6:
        return f'{round(num/1e6,1)}M'
    elif num >= 1e3:
        return f'{round(num/1e3,1)}k'
    else:
        return str(round(num))


""" Templating """
def fetch_query(file_name: str) -> str:
    """Reads the specified SQL query into memory"""
    _file_path = os.path.join(SQL_DIR, file_name)

    with open(_file_path, 'r') as f:
        lines = f.read()
    
    return lines


def clean_params(params: dict) -> dict:
    """Converts the yaml dict values to template-friendly format"""
    for key in params.keys():
        if isinstance(params[key], date_types):
            params[key] = str(params[key])

        if isinstance(params[key], string_types):
            new_value = str(params[key])
            new_value = new_value.replace("'", "''")
            params[key] = f"'{new_value}'"

    return params


def apply_template(file_name: str, params: dict) -> str:
    """ Prepares the SQL query for execution """
    query = fetch_query(file_name)

    if params:
        params = clean_params(params)
        query = query % params
    
    return query


""" Pathing """
def return_data_or_sql(sql_only: bool, connstr: str, file_name: str, params: dict = None):
    """ Choose to return data or simply the SQL """
    
    if sql_only:
        return apply_template(file_name, params)
    else:
        return run_query(connstr, file_name, params)


""" Connection & Execution """
def create_cnxnstr(server: str, database: str) -> str:
    return f"Driver={get_sql_driver()};Server={server};Database={database};Trusted_Connection=yes;"


def get_sql_driver() -> str:
    if system() == "Windows":
        return "{SQL SERVER}"
    else:
        raise NotImplementedError


@query_timing
def run_query(connstr: str, file_name: str, params: dict = None) -> DataFrame:
    """ Read sql template, bind params, and execute read """
    sql = apply_template(file_name, params)

    with connect(connstr) as cnxn:
        df = read_sql_query(sql=sql, con=cnxn)

    return df


def insert_df_to_sql(connstr: str, df: DataFrame, table: str) -> None:
    """ Insert DF to SQL table """
    
    with connect(connstr) as conn:
        q_list  = ','.join(['?' for x in list(df.columns)])
        
        _sql_insert_statement = F"INSERT INTO {table} VALUES ({q_list})"

        with conn.cursor() as cursor:
            cursor.fast_executemany = True
            cursor.executemany(_sql_insert_statement, df.values.tolist())
            cursor.commit()
        print(f'{len(df)} rows inserted to {table}.')

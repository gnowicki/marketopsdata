    SELECT  CONVERT(DATE, ResourceStartDateTime) DataDate
            ,LocationId
            ,ActivityTypeKey
	    ,COUNT(TransferId) Transfers
    FROM    CarvanaDWHS.funnelEvents.tblScheduledTransfers WITH (NOLOCK)
    WHERE   ResourceStartDateTime
    BETWEEN %(start)s AND %(end)s
    GROUP BY CONVERT(DATE, ResourceStartDateTime), LocationId, ActivityTypeKey

SELECT  CONVERT(DATE, ActivityCompletedDateTime) ActivityCompletedDate
       ,ActivityTypeKey
       ,AdvocateEmailAddress
       --,ArrivalDateTimeActual
       --,ArrivalDateTimeExpected
       ,LocationID
       ,ReferenceIdType
       ,ResourceEndDateTime
       ,ResourceStartDateTime
       --,StartJourneyDateTime
       ,Status
       ,StatusReason
       ,StopJourneyDateTime
       ,TransferId
       ,Rejected
       ,RejectionReason
       ,Timezone
       ,Latitude
       ,Longitude
       ,CustomerId
       ,StockNumber
       ,VehicleId
       ,Vin
       ,PurchaseStcReferenceID
       ,PurchaseStcReferenceIDType
       ,PurchaseStcStockNumber
       ,PurchaseStcType
       ,PurchaseStcVin
       ,StcType				  --Retail or Wholesale
       ,CompletionMethod	  --Normal, DrivewayReview, ChaseCar, ThirdPartyTow
       --,Odometer			  -- Vehicle Odo
       --,PurchaseStcOdometer -- Trade-In Odo
  FROM  OpsGroup.marketops.tblCompletedActivity
  WHERE ActivityCompletedDateTime BETWEEN %(start)s AND %(end)s
SELECT  Market LocationName
       ,ForecastMonth
       ,HaulersNeeded
  FROM  OpsGroup.marketops.ProjectedHaulers
  WHERE ForecastMonth BETWEEN %(start)s AND %(end)s
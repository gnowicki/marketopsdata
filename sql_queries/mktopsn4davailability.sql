SELECT SnapshotDate
      ,ResourceDate
      ,LocationDescription
      ,LocationName
      ,Hub
      ,ResourceType
      ,BookableGapduration
      ,BookedResourcesOverbooking
      ,BookedResourcesRegular
      ,hoursbudget
      ,hoursintransit
      ,STC_Split
      ,ActualActivities
      ,numdeliveries Deliveries
      ,numpickups Pickups
      ,numbfps ScheduledBFPs
      ,numreturns Returns
  FROM  Analytics.marketing.MktOpsN4DAvailability
  WHERE ResourceDate BETWEEN %(start)s AND %(end)s
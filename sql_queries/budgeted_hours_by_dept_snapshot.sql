SELECT Market
      ,ProjectionWeek
      ,EmissionsSafetyAutoWash
      ,FirstImpression
      ,FleetMaintenaince
      ,LotManagement
      ,PaperworkManagement
      ,RetailHours
      ,STCHours
      ,TotalDevelopment
      ,VehicleDetailIntake
      ,VehicleQuality
      ,TotalActivities
      ,TotalBFPs
      ,TotalSales
      ,SnapshotDateTime
FROM   OpsGroup.marketops.budgeted_hours_by_dept_snapshot WITH(NOLOCK)
WHERE  ProjectionWeek >= %(start)s
AND    ProjectionWeek <= %(end)s


SELECT LocationName
      ,StaffingMarket
      ,ForecastWeek ForecastDate
      ,SnapshotDate
      ,ForecastedEmployees
      ,JobProfile
      ,NumCurrentEmployees
      ,NumProductiveEmployees
      ,NumInTrainingEmployees
      ,PrevHeadcount
  FROM OpsGroup.marketops.Staffing_Newv2
  WHERE  ForecastWeek >= %(start)s
  AND    ForecastWeek <= %(end)s
  AND    JobProfile in ('STC Advocate', 'Customer Advocate')

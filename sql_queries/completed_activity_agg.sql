SELECT  CONVERT(DATE, ActivityCompletedDateTime) CompletedDate
       ,ActivityTypeKey
       ,LocationID
       ,ResourceEndDateTime
       ,Status
	  ,StcType				  --Retail or Wholesale
       ,CompletionMethod	  --Normal, DrivewayReview, ChaseCar, ThirdPartyTow
       ,COUNT(*) Activities
  FROM  OpsGroup.marketops.tblCompletedActivity
  WHERE   ActivityCompletedDateTime BETWEEN %(start)s AND %(end)s
  GROUP BY  CONVERT(DATE,ActivityCompletedDateTime), ActivityTypeKey, LocationID, ResourceEndDateTime, Status, StcType, CompletionMethod

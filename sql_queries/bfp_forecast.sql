	SELECT Version
		   ,Date
		   ,City
		   ,DeliveryLocationName LocationName
		   ,STCType
		   ,Forecast BFPForecast
		   ,IsCurrent
	FROM   Analytics.marketing.STCForecast
	WHERE  Date BETWEEN %(start)s AND %(end)s
